package de.quandoo.exception;

public class QuandooException extends Exception {

    public QuandooException(String message) {
        super(message);
    }
}
