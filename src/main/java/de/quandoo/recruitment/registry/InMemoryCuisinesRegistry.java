package de.quandoo.recruitment.registry;

import static java.util.Comparator.reverseOrder;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

import de.quandoo.common.util.ArgumentValidator;
import de.quandoo.exception.QuandooException;
import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private static final Logger log = Logger.getLogger(InMemoryCuisinesRegistry.class.getName());

    private final Map<Customer, Cuisine> registry = new ConcurrentHashMap<>();

    @Override
    public void register(final Customer customer, final Cuisine cuisine) {
        try {
            validateParams(customer, cuisine);
        } catch (QuandooException | IllegalArgumentException e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        registry.put(customer, cuisine);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        try {
            ArgumentValidator.validateCuisine(cuisine);
        } catch (QuandooException | IllegalArgumentException e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return registry.entrySet()
                .parallelStream()
                .filter((f) -> f.getValue().getName().equalsIgnoreCase(cuisine.getName()))
                .map(m -> m.getKey())
                .collect(Collectors.toList());
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        try {
            ArgumentValidator.validateCustomer(customer);
        } catch (QuandooException | IllegalArgumentException e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return registry.entrySet()
                .parallelStream()
                .filter((f) -> f.getKey().getUuid().equalsIgnoreCase(customer.getUuid()))
                .map(m -> m.getValue())
                .collect(Collectors.toList());
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        return registry.entrySet()
                .parallelStream()
                .map(m -> m.getValue())
                .collect(groupingBy(identity(), counting()))
                .entrySet()
                .parallelStream()
                .sorted(Map.Entry.comparingByValue(reverseOrder()))
                .map(f -> f.getKey())
                .limit(n)
                .collect(Collectors.toList());
    }

    private void validateParams(Customer userId, Cuisine cuisine) throws QuandooException {
        ArgumentValidator.validateParams(userId, cuisine);
    }
}
