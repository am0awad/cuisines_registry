package de.quandoo.recruitment.common.contants;

public enum CuisineType {

    ITALIAN, FRENCH, GERMAN
}
